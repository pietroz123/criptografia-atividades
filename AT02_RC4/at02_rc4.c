#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX             256
#define CRIPTOGRAFAR    0
#define DESCRIPTOGRAFAR 1

/**
 * HELPER: imprimir um vetor de char
 */
void imprimir(unsigned char *vetor) {
    for (int i = 0; i < MAX; i++)
        printf("%d ", vetor[i]);
    printf("\n\n");
}

/**
 * Converter um texto plano em hexadecimal
 */
char *stringToHex(char *texto) {
    char *hex = (char*) malloc(MAX * sizeof(char)); 

    for (int i = 0, j = 0; i < strlen(texto); ++i, j += 2)
        sprintf(hex + j, "%02x", texto[i] & 0xff);

    return hex;
}

/**
 * Converter um texto em hexadecimal em um texto plano
 */
char *hexToString(char *hex) {
    char *texto = (char*) malloc(MAX * sizeof(char));

    for (int i = 0, j = 0; j < strlen(hex); ++i, j += 2) {
        int val[1];
        sscanf(hex + j, "%2x", val);
        texto[i] = val[0];
        texto[i + 1] = '\0';
    }

    return texto;
}

/**
 * Função de swap
 */
void swap(unsigned char *a, unsigned char *b) {
	char temp = *b;
	*b = *a;
	*a = temp;
}

/**
 * Aqui vai o algoritmo key-scheduling, utilizado para inicializar a permutação no vetor S
 */
void ksa(unsigned char *S, char *key) {

    int keylen = strlen(key);

    // 1) S é inicializado com a permutação identidade
    for (int i = 0; i < MAX; i++) {
        S[i] = i;
    }

    // 2) Permutação com a chave
    int j = 0;
    for (int i = 0; i < MAX; i++) {
        j = (j + S[i] + key[i % keylen]) % MAX;
        swap(&S[i], &S[j]);
    }

}

/**
 * Para todas repetições necessárias, o PRGA modifica o estado e a saída do byte resultante.
 * PRGA: Pseudo-random generation algorithm
 */
unsigned char *prga(unsigned char *S, char *plaintext) {

    unsigned char *ciphertext = (char*) malloc(MAX * sizeof(char));
    memset(ciphertext, 0, MAX);

    int i = 0, j = 0;

    for (int n = 0; n < strlen(plaintext); n++) {

        i = (i + 1) % MAX;
        j = (j + S[i]) % MAX;   // índice de permutação

        swap(&S[i], &S[j]);     // nova permutação

        int t = (S[i] + S[j]) % MAX;    // byte de mascaramento

        // Criptografia
        ciphertext[n] =  plaintext[n] ^ S[t];   // operação XOR

    }

    return ciphertext;

}

int main() {
    
    unsigned char S[MAX];           // vetor de permutações, representa o estado interno do algoritmo
    char key[MAX];                  // chave de comprimento variável, tipicamente de até 2040 bits (256 x 4)
    char plaintext[MAX];            // texto a ser criptografado
    unsigned char ciphertext[MAX];  // conterá o texto criptografado

    int opcao = 0;
    printf("Criptografar (0), Descriptografar (1): ");
    scanf("%d", &opcao);
    getchar();

    switch (opcao)
    {
    case CRIPTOGRAFAR:
        
        printf("Digite o texto a ser criptografado: ");
        scanf("%[^\n]", plaintext);
        getchar();

        printf("Digite a chave: ");
        scanf("%[^\n]", key);
        getchar();

        ksa(S, key);
        strcpy(ciphertext, prga(S, plaintext));

        // Imprime o texto criptografado
        printf("Texto criptografado: ");
        for (int i = 0; i < strlen(ciphertext) || ciphertext[i] != '\0'; i++) {
            printf("%x", ciphertext[i]);
        }

        break;

    case DESCRIPTOGRAFAR:
        
        printf("Digite o texto a ser descriptografado (em hexadecimal): ");
        scanf("%[^\n]", plaintext);
        getchar();

        printf("Digite a chave (em texto plano): ");
        scanf("%[^\n]", key);
        getchar();

        ksa(S, key);
        strcpy( ciphertext, ( prga( S, hexToString(plaintext) ) ) );

        // Imprime o texto criptografado
        printf("Texto descriptografado: ");
        for (int i = 0; i < strlen(ciphertext) || ciphertext[i] != '\0'; i++) {
            printf("%c", ciphertext[i]);
        }
        

        break;
    
    default:
        printf("Opção inválida!\n");
        break;
    }


}