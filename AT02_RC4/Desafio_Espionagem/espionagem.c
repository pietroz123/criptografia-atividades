#include <stdio.h>

/**
 * Extrair os bits
 */
unsigned int extrairBits(unsigned int num, unsigned int inicio, unsigned int fim) {
    return (((1 << inicio) - 1) & (num >> (fim - 1)));
}

/**
 * Máscara
 */
unsigned int mask(int inicio, int quantidade) {
    return ((1 << quantidade) -1) << inicio;
}

int main() {

    unsigned int plaintext = 3665112892;
    // scanf("%u", &plaintext);

    // printf("%d", extrairBits(plaintext, 5, 2));

    unsigned int valor = plaintext & mask(0, 16);
    unsigned int agenciaD   = plaintext & mask(16, 4);

    // unsigned int valor      = extrairBits(plaintext, 16, 1);
    // unsigned int agenciaD   = extrairBits(plaintext, 20, 17);
    // printf("%u\n", plaintext & 0x0000FFFF);
    // unsigned int contaD     = extrairBits(plaintext, 24, 21);
    // unsigned int agenciaO   = extrairBits(plaintext, 28, 25);
    // unsigned int contaO     = extrairBits(plaintext, 32, 29);

    printf("valor: %u\n", valor);
    printf("agenciaD: %u\n", agenciaD);
    // printf("contaD: %u\n", contaD);
    // printf("agenciaO: %u\n", agenciaO);
    // printf("contaO: %u\n", contaO);

}