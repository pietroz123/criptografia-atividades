#include <stdio.h>

int main() {

    FILE *arquivoTexto = fopen("texto1.txt", "r");
	char texto[2000];
    while (fgets(texto, 2000, arquivoTexto) != NULL) { 
        fscanf(arquivoTexto, "%s", texto);
    }

    printf("Texto original:\n");
    printf("%s\n", texto);

    int deslocamento;
    printf("Digite um deslocamento: ");
    scanf("%d", &deslocamento);

    int i = 0;
    while (texto[i] != '\0') {

        int posAlfa = texto[i] - 97;
        int novaPosicao = (posAlfa + deslocamento) % 26;
        texto[i] = novaPosicao + 97;

        i++;
    }

    // Coloca num arquivo o texto cifrado
    fprintf(fopen("cifrado1.txt", "w"), texto);

    printf("Texto cifrado:\n");
    printf("%s\n", texto);



}