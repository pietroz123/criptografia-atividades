#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct letra {
	char letra;
	int quantidade;
    float porcentagem;
} letra;

typedef struct substituicao {
	char letra;
	char subs;
} substituicao;

// =======================================================
// HELPERS
// =======================================================

/**
 * Conta o número de caracteres ignorando os espaços
 */ 
int numeroLetras(char* texto) {
    int count = 0;
    for (int i = 0; i < strlen(texto); i++) {
        if (texto[i] != 32)
            count++;
    }
    return count;
}

/**
 * Função de ordenação por incidência
 */ 
int comparacao_incidencia(const void *a, const void *b) {
	if ( (*(letra*)a).quantidade > (*(letra*)b).quantidade )
		return -1;
	if ( (*(letra*)a).quantidade == (*(letra*)b).quantidade )
		if ( (*(letra*)a).letra < (*(letra*)b).letra )
			return -1;
}

// =======================================================
// FUNÇÕES
// =======================================================

/**
 * Inicializa o vetor de frequências
 */
void inicializaFrequencias(letra* frequencia) {
    for (int i = 0; i < 26; i++) {
		frequencia[i].letra = i + 97;
		frequencia[i].quantidade = 0;
        frequencia[i].porcentagem = 0.0;
	}
}

/**
 * Imprime a frequência de letras
 */
void imprimirFrequencia(letra* frequencia) {
	for (int i = 0; i < 26; i++)
		printf("%c: %d (%f)\n", frequencia[i].letra, frequencia[i].quantidade, frequencia[i].porcentagem);
}

void imprimirOrdem(char ordem[26]) {
	printf("Ordem: ");
	for (int i = 0; i < 26; i++)
		printf("%c ", ordem[i]);
	printf("\n");
}

void imprimirAuxETexto(char *aux, char *texto) {
	printf("%s\n------\n", aux);
	printf("%s\n", texto);
}

void imprimirInicidencia(letra* frequencia) {
	printf("Frequ: ");
	for (int i = 0; i < 26; i++)
		printf("%c ", frequencia[i].letra);
	printf("\n");
}

void inicializarHistorico(substituicao historico[100]) {
	for (int i = 0; i < 100; i++) {
		historico[i].letra = 0;
		historico[i].subs = 0;
	}
}
void imprimirHistorico(substituicao historico[100]) {
	int i = 0;
	printf("\n-----HISTORICO-----\n");
	while (historico[i].letra != 0) {
		printf("%c -> %c\n", historico[i].letra, historico[i].subs);
		i++;
	}
}

/**
 * Conta a quantidade de letras
 */
void analisarFrequencia(letra* frequencia, char* texto) {
    for (int i = 0; i < strlen(texto); i++) {
		int ascii = tolower(texto[i]);
		int posicao = ascii - 97;
		frequencia[posicao].quantidade++;
	}
}

/**
 * Adicionar a porcentagem ao vetor de frequências
 */
void adicionarPorcentagem(letra* frequencia, int n) {
    for (int i = 0; i < 26; i++) {
        frequencia[i].porcentagem = frequencia[i].quantidade != 0 ? (frequencia[i].quantidade / (float) n) * 100 : 0;
    }
}


// =======================================================
// DESCRIPTOGRAFAR
// =======================================================

/**
 * Substitui uma letra no texto por outra letra
 */
void substituir(char *texto, char *aux, char letra, char subs) {
	int i = 0;
	while ( aux[i] != '\0' ) {
		
		if ( aux[i] == letra ) {
			aux[i] = '*'; // marca a letra como 'visitada' 
			texto[i] = toupper(subs);
		}

		i++;

	}	
}



int main(void) {

	char ordem[26] = { 'a', 'e', 'o', 's', 'r', 'i', 'n', 'd', 't', 'm', 'u', 'c', 'l', 'p', 'g', 'q', 'v', 'b', 'f', 'h', 'j', 'z', 'x', 'k', 'y', 'w' };
	
	FILE *arquivoTexto = fopen("cifradoSala2.txt", "r");
	char texto[5000];
    while (fgets(texto, 5000, arquivoTexto) != NULL) { 
        fscanf(arquivoTexto, "%s", texto);
    }

	printf("%s\n", texto);

	letra frequencia[26];
	inicializaFrequencias(frequencia);

	analisarFrequencia(frequencia, texto);
    adicionarPorcentagem(frequencia, numeroLetras(texto));

	printf("\n---------- ORDEM ALFABETICA ----------\n");
    imprimirFrequencia(frequencia);

	printf("\n---------- ORDEM INCIDENCIA ----------\n");
	qsort(frequencia, 26, sizeof(letra), comparacao_incidencia);
	imprimirFrequencia(frequencia);

	char aux[5000];
	strcpy(aux, texto);

	printf("Deseja substituir n letras ou ir uma por uma? (0) Uma por uma\n");
	printf("Opcao: ");
	int qtd;
	scanf("%d", &qtd);

	substituicao historico[100];
	inicializarHistorico(historico);
	int iH = 0;

	if (qtd != 0) {

		int j = 1;
		while ( j <= qtd ) {

			/**
			 * Substitui a nésima letra de maior frequência
			 * pela nésima letra da ordem de incidência
			 */
			substituir(texto, aux, frequencia[j-1].letra, ordem[j-1]);
			historico[iH].letra = frequencia[j-1].letra;
			historico[iH].subs = ordem[j-1];
			iH++;

			j++;
		}

	}
	else {

		char l;	// letra a ser substituída
		char s; // letra de substituição
		int opcao;
		char cTexto[5000];
		char cAux[5000];

		
		imprimirOrdem(ordem);
		imprimirInicidencia(frequencia);
		printf("Substituir: ");
		getchar();
		scanf("%c", &l);
		getchar();
		while (l != '-') {

			printf("Por: ");
			scanf("%c", &s);
			getchar();

			strcpy(cTexto, texto);
			strcpy(cAux, aux);
			substituir(texto, aux, l, s);

			imprimirAuxETexto(aux, texto);

			printf("%c\n", l);
			printf("%c\n", s);


			printf("Essa substituicao fez sentido? (1) Sim (0) Nao\n");
			scanf("%d", &opcao); 
			getchar();
			if (opcao == 0) {
				// Reverte para o estado anterior
				strcpy(texto, cTexto);
				strcpy(aux, cAux);
				printf("Texto revertido\n");
				imprimirAuxETexto(aux, texto);
			}

			historico[iH].letra = l;
			historico[iH].subs = s;
			iH++;

			imprimirOrdem(ordem);
			imprimirInicidencia(frequencia);
			printf("Substituir: ");
			scanf("%c", &l);
			getchar();
		}

	}

	imprimirAuxETexto(aux, texto);	
	imprimirHistorico(historico);

}