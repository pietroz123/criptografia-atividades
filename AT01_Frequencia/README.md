### Funcionamento

Ao iniciar o programa, este irá carregar o texto criptografado fornecido no código. O programa então irá perguntar ao usuário se ele deseja substituir uma letra por vez, ou **n** letras de uma vez. Caso o usuário selecione substituir uma letra por vez, o programa irá perguntar ao usuário qual letra ele deseja substituir por qual letra, e assim sucessivamente até acabarem as letras.
> É importante ressaltar que em ambas as opções o programa irá mostrar ao final o histórico de substituições realizadas pelo usuário.